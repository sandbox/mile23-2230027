ModuleSkeleton
===

What?
---

ModuleSkeleton is not a Drupal module.

ModuleSkeleton is a command-line tool to generate some of the basics of a Drupal 8 module.

It installs easily through Composer.

Install?
---

Install Composer. I suggest this way: [http://mile23.com/content/some-composer-drush-tricks](http://mile23.com/content/some-composer-drush-tricks)

Use Composer to install ModuleSkeleton. Maybe like this:

	composer create-project mile23/moduleskeleton:dev-master

Or maybe like this:

	composer require mile23/moduleskeleton:dev-master

Here's ModuleSkeleton's page on packagist.org: https://packagist.org/packages/mile23/moduleskeleton


Use?
---

`cd` into the moduleskeleton directory, and then do something like this:

	.app/moduleskeleton generate module_machine_name path/to/place/module

It does stuff, and then you're done.

You now have an .info.yml file, a .module file, a PHPUnit test, and a very basic SimpleTest test. The tests should pass at this point. All of this is wrapped in the proper directory structure.


Develop?
---

Want to help out and add features? Jump right in.
