<?php

/**
 * @file
 * Contains ModuleDirectoryGenerator
 */

namespace Mile23\ModuleSkeleton;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Build module elements.
 *
 * Note that all paths are arrays, which will be joined with delimiters.
 */
class ModuleBuilder {

  protected $name;
  protected $readable;

  /**
   * Root path in which the module is to be constructed.
   *
   * Do not include trailing slashes or deliters.
   *
   * @var string
   */
  protected $path;
  protected $output;

  public function __construct($name, $readable_name, $path, OutputInterface $output) {
    $this->name = $name;
    $this->readable = $readable_name;
    $this->path = $path;
    $this->output = $output;
  }

  /**
   * Generate a path from module's root path.
   *
   * @param string $path
   * @param int $permission
   *
   * @return string
   *   The path generated.
   */
  public function generatePath($path, $permission = 0777) {
    if (!file_exists($this->path)) {
      throw new \Exception("Parent path doesn't exist.");
    }
    $built_path = $this->rootPath();
    if (!file_exists($built_path)) {
      \mkdir($built_path, $permission);
      $this->output->writeln('Generating module root path: ' . $built_path);
    }
    $path_elements = explode('/', $path);
    foreach ($path_elements as $path_part) {
      $built_path .= '/' . $path_part;
      if (!file_exists($built_path)) {
        \mkdir($built_path, $permission);
      }
    }
    return $built_path;
  }

  public function addInfoFile($yaml) {
    $this->putFileAtModulePath(
      '', $this->name . '.info.yml', $yaml
    );
  }

  public function addModuleFile($module) {
    $this->putFileAtModulePath(
      '', $this->name . '.module', $module
    );
  }

  public function addSimpleTest($filename, $test) {
    $path = implode('/', array(
      'lib',
      $this->namespacePath(),
      'Tests',
    ));
    $this->putFileAtModulePath($path, $filename, $test);
  }

  public function addPhpUnitTest($filename, $test) {
    $path = implode('/', array(
      'tests',
      $this->namespacePath(),
      'Tests',
    ));
    $this->putFileAtModulePath($path, $filename, $test);
  }

  protected function putFileAtModulePath($path, $filename, $contents) {
    $full_path = $this->generatePath($path);
    file_put_contents($full_path . '/' . $filename, $contents);
  }

  public function generateStructure($permission = 0777) {
    $this->output->writeln('Generating module file structure...');
    foreach ($this->treeStructure() as $path) {
      $this->output->writeln('path: ' . $path);
      $this->generatePath($path, $permission);
    }
  }

  protected function rootPath() {
    return $this->path . '/' . $this->name;
  }

  protected function namespacePath() {
    return 'Drupal/' . $this->name;
  }

  protected function libPath() {
    return implode(
      '/', array(
        'lib',
        $this->namespacePath(),
      )
    );
  }

  protected function unitTestPath() {
    return implode(
      '/', array(
        'tests',
        $this->namespacePath(),
        'Tests',
      )
    );
  }

  protected function functionalTestPath() {
    return implode(
      '/', array(
        'lib',
        $this->namespacePath(),
        'Tests',
      )
    );
  }

  protected function treeStructure() {
    $paths = array(
      $this->libPath(),
      $this->unitTestPath(),
      $this->functionalTestPath(),
    );
    return $paths;
  }

}
