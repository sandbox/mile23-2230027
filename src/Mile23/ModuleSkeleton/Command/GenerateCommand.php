<?php

/**
 * @file
 * Command file for ModuleSkeleton.
 */

namespace Mile23\ModuleSkeleton\Command;

use Mile23\ModuleSkeleton\ModuleBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class GenerateCommand extends Command {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('generate')
      ->setDescription('Generate a skeleton module.')
      ->addArgument(
        'name', InputArgument::REQUIRED, 'Machine name for the module.'
      )
      ->addArgument(
        'path', InputArgument::REQUIRED, 'Path where module will be built. Must be a directory.'
      )
      ->addOption(
        'readable', 'r', InputOption::VALUE_OPTIONAL, 'Human-readable module name. Defaults to capitalized machine name.'
      );
/*      ->addOption(
        'dependencies', 'd', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL, 'Module dependencies.'
      );*/
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    // Check our arguments.
    $path = realpath($input->getArgument('path'));
    if (!is_dir($path)) {
      throw new \Exception('bad path');
    }
    $name = $input->getArgument('name');
    if (file_exists($path . '/' . $name)) {
      throw new \Exception('file exists already.');
    }
    $readable_name = $input->getOption('readable');
    if ($readable_name == '') {
      $readable_name = ucfirst($name);
    }

    // Make some directories.
    $builder = new ModuleBuilder($name, $readable_name, $path, $output);
    $builder->generateStructure(0777);

    // Make an .info.yml file.
    $output->writeln('Generating .info.yml file.');
    $info = array(
      'name' => $readable_name,
      'type' => 'module',
      'description' => 'CHANGE THIS.',
      'package' => 'CHANGE THIS.',
      'version' => 'VERSION',
      'core' => '8.x',
    );
    $yaml = Yaml::dump($info, 2);
    $builder->addInfoFile($yaml);

    // Make a .module file.
    $output->writeln('Generating .module file.');
    $module = file_get_contents(__DIR__ . '/../../../../templates/module_file_template.php');
    $module = str_replace('{{modulename}}', $name, $module);
    $builder->addModuleFile($module);

    // Make a SimpleTest test file.
    $output->writeln('Generating SimpleTest test file.');
    $template = file_get_contents(__DIR__ . '/../../../../templates/simpletest_template.php');
    $template = str_replace('{{modulename}}', $name, $template);
    $template = str_replace('{{Modulename}}', $readable_name, $template);
    $builder->addSimpleTest($readable_name . 'Test.php', $template);

    // Make a PHPUnit test file.
    $output->writeln('Generating PHPUnit test file.');
    $template = file_get_contents(__DIR__ . '/../../../../templates/phpunit_template.php');
    $template = str_replace('{{modulename}}', $name, $template);
    $template = str_replace('{{Modulename}}', ucfirst($name), $template);
    $builder->addPhpUnitTest($readable_name . 'UnitTest.php', $template);

  }

}
